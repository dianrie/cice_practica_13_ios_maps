//
//  MapViewController.swift
//  cice_practica_13_iOS_maps
//
//  Created by Diego Angel Fernandez Garcia on 3/3/19.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {
    var direccion: String? = nil
    
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.showsCompass = true
        mapView.isZoomEnabled = true
        
        title = direccion
        configuragarMapa(localizacion: direccion!)
        
        // Do any additional setup after loading the view.
    }
    func configuragarMapa(localizacion: String){
        //Obtener localización geográfica a través de un Geocoder
        let geocoder = CLGeocoder()
        //Solicitamos a Apple el Placemark utilizando la localización en formato String
        geocoder.geocodeAddressString(localizacion) { (placemarks, error) in
            //Comprobamos si hay respuesta utilizando el parámetro error
            if let error = error{
                print(error.localizedDescription)
                self.title = "No se encotro dirección"
                //Alerta
                       let alerta = UIAlertController(title: "Atención", message: "No se encontro la dirección", preferredStyle: .actionSheet)
                       //Cancelar
                       let cancelar = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                        alerta.addAction(cancelar)
                    //Nueva busqueda
                let aceptar = UIAlertAction(title: "Nueva Busqueda", style: UIAlertAction.Style.default, handler: { (volver) in
                    self.navigationController?.popToRootViewController(animated: true)
                })
                //PopOver en el iPad
                
                if let popoverController = alerta.popoverPresentationController{
                                popoverController.sourceView = self.view
                                //popoverController.sourceRect = self.mapView.bounds
                                popoverController.sourceRect = CGRect(x: 500, y: 500, width: 0, height: 0)
                       }
                
                 alerta.addAction(aceptar)
                        self.present(alerta, animated: true, completion: nil)
                return
            }
            //Comprobamos si el array de placemarks no es nill (contiene datos)
            if let placemarks = placemarks {
                //Sacamos el primer resultado del array
                let placemark = placemarks[0]
                
                //Utilizamos el Placemark vamos a crear una chincheta (annotation)
                let annotation = MKPointAnnotation()
                
                //Comprobamos que el Placemark tenga coordenadas geográficas
                if let location = placemark.location{
                    //Mostramos la chincheta en el mapa
                    //Asignamos al annotation las coordenadas del placemark
                    annotation.coordinate = location.coordinate
                    self.mapView.addAnnotation(annotation)
                    //Colocar el mapa con una región concreta
                    let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 250, longitudinalMeters: 250)
                    //Decimos al mapa que se ajusta a dicha region
                    self.mapView.setRegion(region, animated: true)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
