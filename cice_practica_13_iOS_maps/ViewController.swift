//
//  ViewController.swift
//  cice_practica_13_iOS_maps
//
//  Created by Diego Angel Fernandez Garcia on 3/3/19.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var labelBuscar: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "buscar" {
            if let viewController = segue.destination as? MapViewController {
                viewController.direccion = labelBuscar.text
               
            }
        }
    }
}

